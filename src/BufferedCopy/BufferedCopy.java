package BufferedCopy;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class BufferedCopy {
	public static void main(String[] args) throws IOException {
	       BufferedReader inputStream = null;
	        PrintWriter outputStream = null;
	        try {
	            inputStream = 
	                new BufferedReader(new FileReader("commedia.txt"));
	            outputStream = 
	                new PrintWriter(new FileWriter("characteroutput.txt"));
	            String c;
	            while ((c = inputStream.readLine()) != null) {
	                outputStream.println(c);
	            }
	        } finally {
	            if (inputStream != null) {
	                inputStream.close();
	            }
	            if (outputStream != null) {
	                outputStream.close();
	            }
	        }
	}
}
